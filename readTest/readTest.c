#include <stdio.h>
#include <stdlib.h>
#include <string.h>



static char* getfield(char* line, int num)
{
    char* tok;
    for (tok = strtok(line, ";"); tok && *tok; tok = strtok(NULL, ";\n"))
    {
      if (!--num)
        return tok;
    }
    return NULL;
}

static int video_decode_test(char *filename)
{
    FILE *files;
    char file1[64];
    char file2[64];
    char file3[64];
    char file4[64];
    char file5[64];
    int count = 0;

    //read in file names
   if((files = fopen("config", "rb")) == NULL)
      return -2;

   else
   {
     //fscanf(files,"%[^;]s %[^;]s %[^;]s %[^;]s %[^;]s",file1,file2,file3,file4,file5);   
    fscanf(files,"%s %s %s %s %s",file1,file2,file3,file4,file5);   


      printf("file1: %s\n", file1);
      printf("file2: %s\n", file2);
      printf("file3: %s\n", file3);
      printf("file4: %s\n", file4);
      printf("file5: %s\n", file5);

   }

}

int main (int argc, char **argv)
{
   if (argc < 2) {
      printf("Usage: %s <filename>\n", argv[0]);
      exit(1);
   }

   return video_decode_test(argv[1]);
}